# DevOps API
The purpose of this repo is a small API to allow the following functions:

* Provide an endpoint to take the cloud-init phonehome notification on EC2 instances, then reformat and forward it to a slack webhook.
* Provide a way for new docker swarm cluster members to push/get join tokens. These are used by script new EC2 instances use during the final stages of cloud-init.
