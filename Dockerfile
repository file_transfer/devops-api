FROM node:10-alpine

WORKDIR /app

COPY ./lib ./lib
COPY ./package* ./
COPY ./index.js .

RUN npm install --production  \
    && chown -R node:node /app

ENTRYPOINT npm start