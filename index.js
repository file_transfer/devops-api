const express = require('express'),
      app = express(),
      {validateParams} = require('./lib/misc'),
      {createLogger, transports, format} = require('winston'),
      hostForm =  require('./lib/formatter'),
      request = require('request-promise-native'),
      Keyv = require('keyv'),
      {randomBytes} = require('crypto');

const webhook = process.env.SLACK_WEBHOOK,
      port    = process.env.HTTP_PORT,
      logLevel = (process.env.LOG_LEVEL) ? process.env.LOG_LEVEL : 'info',
      domainName = process.env.DOMAIN_NAME,
      keyv = new Keyv();

var locked = false;

const logger = createLogger({
  format: format.combine(
    hostForm(),
    format.timestamp(), 
    format.json(),
  ),
  level: logLevel,
  transports: new transports.Console(),
})

process.on('SIGINT', handle);
process.on('SIGTERM', handle);
function handle(signal) {
  logger.info(`Received ${signal}`);
  process.exit(0);
}

app.use(express.json());
app.use(express.urlencoded({extended: false}));

/*
 * Endpoint for cloud-init phone home of EC2 instances. 
 * Changes phone home url-encoded POST to JSON POST and forwards to Slack webhook.
 */

app.get('/instance', async (req, res) => {
  res.send('OK');
});

app.post('/instance', async (req, res) => {
  res.send('OK');
  let clientIp = req.ip.replace(/::ffff:/g, '');
  logger.info(`Request received from ${clientIp}`);

  let validate = validateParams(req.body, ['instance_id'])
  if (validate) return logger.error(`Body missing param: "${validate}"`);
  
  let postBody = '*New server online*\n';
  postBody += `Instance ID: ${req.body.instance_id}\n`;
  postBody += `SSH addr: \`ssh ubuntu@${req.body.instance_id}.${domainName}\`\n`;

  let opts = {
    method: 'POST',
    uri: webhook,
    json: true,
    body: { text: postBody},
    resolveWithFullResponse: true,
  };
  try {
    let slackResp = await request.post(opts)
    logger.debug(`Response from slack: code=${slackResp.statusCode}`)
  } catch (err) {
    logger.error(`Error with slack webhook: ${err}`);
  }
});

/*
 * Endpoint for new swarm instances to get docker swarm join tokens
 */
app.post('/swarm/lock', async (req, res) => {
  if (locked) return res.status(409).json({error : 'Swarm locked'});

  let validate = validateParams(req.body, ['address'])
  if (validate) {
    let msg = `Body missing param: '${validate}'`;
    logger.error(msg);
    return res.status(400).json({error : msg});
  }
 
  locked = true;
  let authToken = randomBytes(32).toString('hex');
  res.status(202).json({authToken: authToken});
  await keyv.set('authToken', authToken);
  await keyv.set('swarmAddr', req.body.address);
});

app.post('/swarm/token', async (req, res) => {
  let tokenIsThere = validateParams(req.headers, ['x-auth-token'])
  if (tokenIsThere) {
    logger.error(`Missing auth header.`);
    return res.status(401).json({error: 'Unauthorized'});
  }

  let authToken = await keyv.get('authToken');
  if (authToken !== req.headers['x-auth-token']) {
    logger.error(`Unauthorized`);
    return res.status(401).json({error: 'Unauthorized'});  
  }

  let validate = validateParams(req.body, ['Manager', 'Worker'])
  if (validate) {
    let msg = `Body missing param: '${validate}'`;
    logger.error(msg);
    return res.status(400).json({error : msg});
  }
  res.status(202).end();
  await keyv.set('managerToken', req.body.Manager);
  await keyv.set('workerToken', req.body.Worker);
});

app.get('/swarm/token', async (req, res) => {
  let managerToken = await keyv.get('managerToken');
  let workerToken = await keyv.get('workerToken');
  let Address = await keyv.get('swarmAddr');

  if ((! managerToken) || (! workerToken) || (! Address)) return res.json({})

  res.json({
    Manager: managerToken,
    Worker: workerToken,
    Address: Address,
  });
});

app.listen(port, () => {
  logger.info(`Starting API server on port ${port}`);
});
