
function validateParams(body, params) {
  // req.body is not an instance of Object
  let bodyArr = Object.keys(body)
  for (let key of params) {
    if (bodyArr.indexOf(key) < 0) {
      return key;
    }
  }
  return '';
};

module.exports = {
  validateParams: validateParams,
}