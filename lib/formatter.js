// Winston logs formatter to include hostname
const {format} = require('winston'),
      os = require('os');

const hostname = os.hostname();

module.exports = format((info) => {
  info.hostname = hostname;
  return info;
});