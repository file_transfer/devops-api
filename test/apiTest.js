const test = require('ava'),
      request = require('request-promise-native');

const base_url = `http://localhost:${process.env.HTTP_PORT}/`;
var authToken = '';

test('Test /instance GET', async(t) => {
  t.plan(1);
  let opts = {
    method: 'GET',
    uri: base_url + 'instance',
    resolveWithFullResponse: true,
    simple: false,
  };
  let resp = await request(opts);
  t.is(resp.statusCode, 200, 'Status code was not 200');
})

test.serial('Test /swarm/token GET w/ no value', async(t) => {
  t.plan(2);
  let opts = {
    method: 'GET',
    uri: base_url + 'swarm/token',
    json: true,
    resolveWithFullResponse: true,
    simple: false,
  };
  let resp = await request(opts);
  t.is(resp.statusCode, 200, 'Status code was not 200');
  t.deepEqual(resp.body, {}, 'Body was not correct');
})


test.serial('Test /swarm/token POST w/o auth key', async(t) => {
  t.plan(1);
  let opts = {
    method: 'POST',
    uri: base_url + 'swarm/token',
    json: true,
    body: { test: 'test' },
    resolveWithFullResponse: true,
    simple: false,
  };
  let resp = await request(opts);
  t.is(resp.statusCode, 401, 'Status code was not 401');
})

test.serial('Test /swarm/lock POST', async(t) => {
  t.plan(2);
  let opts = {
    method: 'POST',
    uri: base_url + 'swarm/lock',
    json: true,
    body: { address: '1234' },
    resolveWithFullResponse: true,
    simple: false,
  };
  let resp = await request(opts);
  t.is(resp.statusCode, 202, 'Status code was not 202');
  t.truthy(resp.body.authToken, 'Auth token missing');
  authToken = resp.body.authToken;
})

test.serial('Test /swarm/lock POST cant lock on 2nd post', async(t) => {
  t.plan(1);
  let opts = {
    method: 'POST',
    uri: base_url + 'swarm/lock',
    json: true,
    body: { address: '4321' },
    resolveWithFullResponse: true,
    simple: false,
  };
  let resp = await request(opts);
  t.is(resp.statusCode, 409, 'Status code was not 409');
})

test.serial('Test /swarm/token POST', async(t) => {
  t.plan(1);
  let opts = {
    method: 'POST',
    uri: base_url + 'swarm/token',
    json: true,
    body: { 
      Manager: '1234567890',
      Worker: '0987654321',
     },
    headers: { 'X-auth-token': authToken },
    resolveWithFullResponse: true,
    simple: false,
  };
  let resp = await request(opts);
  t.is(resp.statusCode, 202, 'Status code was not 202');
})

test.serial('Test /swarm/token GET', async(t) => {
  t.plan(2);
  let expected = {
    Manager: '1234567890',
    Worker: '0987654321',
    Address: '1234',
  }
  let opts = {
    method: 'GET',
    uri: base_url + 'swarm/token',
    json: true,
    resolveWithFullResponse: true,
    simple: false,
  };
  let resp = await request(opts);
  t.is(resp.statusCode, 200, 'Status code was not 200');
  t.deepEqual(resp.body, expected, 'Body was not correct');
})